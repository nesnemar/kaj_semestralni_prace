

/** object with variables used to keep references on html elements **/
const elements = {

//this is where timer appears
    Output : document.querySelector("#Time"),
    body : document.body,

//references on objects which hide in in different occasions
    MenuItems : document.querySelectorAll(".MenuItem"),
    DifficultyButtons : document.querySelectorAll(".Difficulty"),
    EasyButton: document.querySelector("#Easy"),
    MediumButton: document.querySelector("#Medium"),
    HardButton: document.querySelector("#Hard"),
    StartButton: document.querySelector("#start"),
    LeaderboardsButton: document.querySelector("#leaderboards"),
    CloseLeaderboardsButton: document.querySelector("#CloseWindow"),

    Character : document.querySelector(".Character"),
    Slogan : document.querySelector(".Slogan"),
    Logo : document.querySelector("#magnify"),
    CharacterText : document.querySelector("#text"),

//this is where images spawn
    canvas : document.querySelector("#imageOutput"),

//leaderboard elements
    leaderboards : document.querySelector("#topFive"),
    easyList : document.querySelector("#EasyList"),
    mediumList : document.querySelector("#MediumList"),
    hardList : document.querySelector("#HardList"),

//form when player wins the game
    winnerArea : document.querySelector("#WinnerInput")
}


elements.EasyButton.addEventListener("click", () => startGame("Easy"));
elements.MediumButton.addEventListener("click", () => startGame("Medium"));
elements.HardButton.addEventListener("click", () => startGame("Hard"));

elements.StartButton.addEventListener("click", () => revealDifficulty());
elements.LeaderboardsButton.addEventListener("click", () => loadResults());
elements.CloseLeaderboardsButton.addEventListener("click", ()=> closeLeaderboards());


//when document loads, slogan should appear on top of the screen
document.addEventListener("readystatechange", () => elements.CharacterText.innerHTML = 	pickRandomMessage(positiveMessages) + " - Andrej Babiš");

//logo image is used as "return to menu", works only if difficulty buttons are visible
elements.Logo.addEventListener("click", ()=>{

    if(document.querySelector(".Difficulty").classList.contains("Reveal")){
        toggleVisibility(elements.DifficultyButtons);
        toggleVisibility(elements.MenuItems);
    }
})





/** arrays of quotes, which appear on top of the screen **/
const positiveMessages = [
    '"Pohoda"',
    '"Koblihaaaaa"',
    '"A já vám pindíka ukazovat nebudu."',
    '"Neumím dobře mluvit."',
    '"Nejlepší rozpočet této země..."',
    '"A ty včely mají rády tu řepku, můžou si kecat novináři co chtějí."'

]

const negativeMessages = [
    '"Je to podvrh, je to kampaň... Já to odmítám."',
    '"Sorry jako..."',
    '"Neodejdu z politiky, takovou radost vám neudělám!"',
    '"My chceme dovládnout, demise je nesmysl."',
    '"Vy jste jedna zkorumpovaná pakáž!"'
]
