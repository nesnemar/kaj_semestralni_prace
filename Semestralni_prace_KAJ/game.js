

/** class representing whole game and all phases happening while in progress **/
class Game {

    constructor(TimeLimit,difficulty, gameObjects,mobileDevice) {
        this.Timer = TimeLimit;
        this.StartingTime = TimeLimit;
        this.gameFinished = false;
        this.difficulty = difficulty;
        this.gameObjects = gameObjects;
        this.gameObjectsUsed = 0;
        this.winner = false;
        this.mobileDevice = mobileDevice;
    }

    /** prepare everything for initialization of game **/
    init(){

        //hide all form buttons
        elements.body.classList.toggle("InProgress");
        elements.Logo.classList.toggle("HideWhenInGame");


        //clear div, where images spawn and close leaderboards (just in case...)
        elements.canvas.innerHTML = "";
        closeLeaderboards();

        //begin game cycle in function gameOrder
        this.Countdown = setInterval(() => this.gameOrder(),1000);

        //hide "Babis" image and slogan on top of screen
        toggleVisibility(elements.DifficultyButtons);
        toggle2Classes(elements.Character,"HideCharacter","RevealCharacter");
        toggle2Classes(elements.Slogan,"HideSlogan","RevealSlogan");


        //radius of mask through blurred picture
        let radius;
        if (this.difficulty === "Easy") {
            radius = 130;
        } else if (this.difficulty === "Medium") {
            radius = 100;
        } else {
            radius = 60;
        }


        //jQuery function used to track cursor position, also used to create dynamic mask through blurred image
        $(document).mousemove(function (event) {

            if(!this.mobileDevice){
                let posX = event.pageX;
                let posY = event.pageY;

                $(".Fit").css({'mask-image': "radial-gradient(" + "circle at " + (posX) + 'px' + ' ' + (posY) + 'px' + "," + "transparent " + radius + "px, white 0%)"});
                $(".Fit").css({'-webkit-mask-image': "radial-gradient(" + "circle at " + (posX) + 'px' + ' ' + (posY) + 'px' + "," + "transparent " + radius + "px, white 0%)"});
            }

        });

    }


    /** defines game order (when game should end and what should happen in each timer iteration **/
    async gameOrder(){

        if(!this.gameFinished){
            this.updateTime();
            this.setupGameObjects();

            if(this.gameFinished){
                await this.endGame();
            }

        } else {
            await this.endGame();
        }
    }



    /** this function represents everything, what should happen when all objects are found or timer is up **/
    async endGame(){


        let isWinner = this.winner;

        elements.canvas.innerHTML = "";

        elements.body.classList.toggle("InProgress");

        toggle2Classes(elements.Character,"HideCharacter","RevealCharacter");
        toggle2Classes(elements.Slogan,"HideSlogan","RevealSlogan");

        clearInterval(this.Countdown);
        let finalTime = await this.Timer;
        let startTime = await this.StartingTime;


        //if player wins, form should appear and winner must enter his name (simple validation for empty string) also positive message appears on top
        if(isWinner) {

            let result = startTime - finalTime;
            console.log("You are winner and Your time is: " + result);
            this.Timer = this.StartingTime;

            document.getElementById("buttonLocation").innerHTML = "<button id='winButton' name='Submit' type='button'><b>Submit</b></button>";


            let winButton = document.querySelector("#winButton");

            winButton.addEventListener("click", (e) => this.saveResult(e,result));

            elements.winnerArea.classList.remove("HideOnStart");
            elements.winnerArea.classList.toggle("Reveal");
            elements.winnerArea.classList.toggle("Hide");

            elements.CharacterText.innerHTML = pickRandomMessage(positiveMessages)  + " - Andrej Babiš";
            elements.Output.innerHTML = "";

            //if player lost the game, menu should appear
        } else {
            toggleVisibility(elements.MenuItems);
            elements.CharacterText.innerHTML = pickRandomMessage(negativeMessages)  + " - Andrej Babiš";
            elements.Logo.classList.toggle("HideWhenInGame");
            elements.Output.innerHTML = "<b id='timer'>Time's up!</b>";


        }
    }


    /** function used to show timer on screen **/
    updateTime(){

        if(this.Timer > 0) {

            this.Timer--;

            if(this.Timer < 6){
                elements.Output.style.color = "red";
            } else {
                elements.Output.style.color = "black";
            }

            let Minutes = (Math.floor(this.Timer / 60)).toString();
            let Seconds = (this.Timer - (Minutes * 60)).toString();

            if (Minutes.length < 2) {
                Minutes = "0" + Minutes;
            }
            if (Seconds.length < 2) {
                Seconds = "0" + Seconds;
            }

            elements.Output.innerHTML = "<b id='timer'>" + Minutes + ":" + Seconds + "</b>";

        } else {
            this.winner = false;
            this.gameFinished = true;
        }

    }



    /** this function creates game objects in game area **/
    setupGameObjects(){

        //check if no other image is present in image output HTML element
        if (elements.canvas.innerHTML.trim().length === 0) {

            let img = new Image();

            img.onload = function() {
                elements.canvas.appendChild(img);
            };

            let sizeOfIMG;

            if(document.body.clientHeight >= document.body.clientWidth){
                sizeOfIMG = document.body.clientHeight;
            } else {
                sizeOfIMG = document.body.clientWidth;
            }

            //if player uses mobile device, image sizes are different - mobile version doesnt include blurred image and mask
            if(this.mobileDevice){
                if(this.difficulty === "Easy") {

                    img.style.width = sizeOfIMG/6 + "px";

                } else if(this.difficulty === "Medium") {

                    img.style.width = sizeOfIMG/7 + "px";

                } else {

                    img.style.width = sizeOfIMG/8 + "px";
                }
            } else {
                img.style.width = sizeOfIMG/8 + "px";
            }

            img.classList.add("toBeFound");
            img.style.position = "absolute";

            //sets image on random place on screen
            img.style.top = Math.floor(Math.random() * 70) + 1 + "%";
            img.style.right = Math.floor(Math.random() * 70) + 1 + "%";
            img.style.zIndex = "2";
            img.style.cursor = "pointer";
            img.alt = "to be found.";

            let sources = [];

            sources.push(this.gameObjects[this.gameObjectsUsed].getImgURL());
            sources.push(this.gameObjects[this.gameObjectsUsed].getAudioURL());

            img.src = sources[0];

            //adds event handler when user clicks on imag -> sound should be played and image should disappear
            img.addEventListener("click", ()=>this.imageEventHandler(sources[1]));

            this.gameObjectsUsed++;

        }
    }


    /** this function handles events when user clicks on image **/
    async imageEventHandler(musicURL){

        elements.canvas.innerHTML = "";

        //if all images were used, game should end
        if(this.gameObjectsUsed === this.gameObjects.length){
            this.winner = true;
            this.gameFinished = true;
            await this.endGame();
        }

        let audio = new Audio(musicURL);
        await audio.play();

    }


    /** this function is used after player wins game and clicks on submit when form appears
     *  -> players name should now appear in leaderboards if he was fast enough **/
    saveResult(event,time){

        let input = document.getElementById("winInput");

        let inputValue = input.value;


        //check for empty input
        if(inputValue.trim() === ""){
            event.preventDefault();
            console.log("prohibited");

            input.style.borderColor = "red";

        } else {

            //if any results are present... localStorage items are saved in arrays with names representing games difficulty
            if (window.localStorage.getItem(this.difficulty)) {

                let result = window.localStorage.getItem(this.difficulty);
                console.log(result)
                result = JSON.parse(result);

                result.push({name: inputValue, time: time});


                //check if array is really just "top 5", if array is bigger, pop last item from time sorted array
                if (result.length > 5) {
                    result.sort((a, b) => (a.time) - (b.time));
                    result.pop();
                } else {
                    result.sort((a, b) => (a.time) - (b.time));
                }

                window.localStorage.setItem(this.difficulty, JSON.stringify(result));

            } else {
                let result = [];
                result.push({name: inputValue, time: time});

                window.localStorage.setItem(this.difficulty, JSON.stringify(result));
            }


            input.value = "";
            input.style.borderColor = "black";

            elements.winnerArea.classList.toggle("Reveal");
            elements.winnerArea.classList.toggle("Hide");

            toggleVisibility(elements.MenuItems);

            elements.Logo.classList.toggle("HideWhenInGame");

        }
    }
}
