
/** class representing any game image spawned on screen when in game **/
class GameObject {
    constructor(imageURL, name, audioURL) {
        this.imageURL = imageURL;
        this.name = name;
        this.audioURL = audioURL
    }

    getImgURL(){
        return this.imageURL;
    }

    getAudioURL(){
        return this.audioURL;
    }
}
