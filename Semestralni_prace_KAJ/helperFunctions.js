

/** function used to start game
 * takes parameters - isDevice with touchscreen, time limit, chosen difficulty and objects, which should be used as game objects **/
function startGame(difficulty){
    const game = new Game(60, difficulty, createGameObjects(), isDeviceWithTouchscreen());
    game.init();
}


/** function which provides all game object names and source addresses, then shuffles items in array **/
function createGameObjects(){

    let data = [];

    for(let i = 1; i < 13; i++){
        data.push(new GameObject("Resources/GameObjects/"+ i + ".png",i, "Resources/Sound/" + i + ".m4a"));
    }

    data = shuffleObjects(data);

    return data;
}

/** function shuffling array, so game object order isnt always the same **/
function shuffleObjects(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return array;
}


/** helper function, which toggles any 2 classes for 1 element **/
function toggle2Classes(element,firstClass,secondClass){
    element.classList.toggle(firstClass);
    element.classList.toggle(secondClass);
}

/** helper function, which hides and reveals objects in app
 *  (all classes represent state, in which are html elements visible or hidden - menu buttons, difficulty buttons...)
 *  Hide on start represents classes, which are hidden on app start (difficulty buttons for example)...
 *  Used for array of objects
 *  **/
function toggleVisibility(items){
    items.forEach(item =>{
        item.classList.toggle("Hide");
        item.classList.toggle("Reveal");
        item.classList.remove("HideOnStart");
    });
}

/** function revealing difficulty buttons **/
function revealDifficulty(){
    toggleVisibility(elements.DifficultyButtons);
    toggleVisibility(elements.MenuItems);
}



/** selects random message from array of messages **/
function pickRandomMessage(messageArray){
    return messageArray[(Math.floor(Math.random() * messageArray.length) + 1) -1];
}


/** function used to check, if client uses touchscreen device, blurred image is automatically hidden then **/
function isDeviceWithTouchscreen(){
    const userAgent = navigator.userAgent;

    let mobileDevice;

    if(/android/i.test(userAgent)){

        console.log("Game works differently on this device");
        document.querySelector(".Fit").style.visibility = "hidden";
        mobileDevice = true;

    }
    else if(/iPad|iPhone|iPod/i.test(userAgent)){

        console.log("Game works differently on this device");
        document.querySelector(".Fit").style.visibility = "hidden";
        mobileDevice = true;

    } else {
        mobileDevice = false
    }
    return mobileDevice;
}

