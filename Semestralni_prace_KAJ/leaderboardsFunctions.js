
/** function used to load data for leaderboards from localStorage **/
function loadResults(){

    let counter = 0;

    elements.leaderboards.classList.remove("HideOnStart");
    toggle2Classes(elements.leaderboards,"HideBoard","RevealBoard");

    let result;

    //renders data from array if data are present
    if(window.localStorage.getItem("Easy")){
        result = window.localStorage.getItem("Easy");
        result = JSON.parse(result);

        elements.easyList.innerHTML = "<h3>Easy</h3>";

        result.forEach(item => {
            counter++;
            elements.easyList.innerHTML += "<li><p class='easy'>"+ counter + ". " + item.name.toUpperCase() + " </p><p class='data'>Finished in: " + "<b>" +item.time + "s.</b></p></li>"
        })
    } else {
        elements.easyList.innerHTML = "<h3>Easy</h3>";
        elements.easyList.innerHTML += "<div  class='error'><b>Nothing to show... Play few games and get here ;)</b></div>"
    }

    if(window.localStorage.getItem("Medium")){

        result = window.localStorage.getItem("Medium");
        result = JSON.parse(result);

        elements.mediumList.innerHTML = "<h3>Medium</h3>";

        counter = 0;

        result.forEach(item => {
            counter++;
            elements.mediumList.innerHTML += "<li><p class='medium'>"+ counter + ". " + item.name.toUpperCase() + " </p><p class='data'>Finished in: " + "<b>" + item.time + "s.</b></p></li>"
        })
    } else {
        elements.mediumList.innerHTML = "<h3>Hard</h3>";
        elements.mediumList.innerHTML += "<div  class='error'><b>Nothing to show... Play few games and get here ;)</b></div>"
    }

    if(window.localStorage.getItem("Hard")){
        result = window.localStorage.getItem("Hard");
        result = JSON.parse(result);

        elements.hardList.innerHTML = "<h3>Medium</h3>";

        counter = 0;

        result.forEach(item => {
            counter++;
            elements.hardList.innerHTML += "<li><p class='hard'>"+ counter + ". " + item.name.toUpperCase() + " </p><p class='data'>Finished in: " + "<b>" + item.time + "s.</b></p></li>"
        })
    } else {
        elements.hardList.innerHTML = "<h3>Hard</h3>";
        elements.hardList.innerHTML += "<div  class='error'><b>Nothing to show... Play few games and get here ;)</b></div>"
    }
}


/** function, which is used to hide leaberboards, when user clicks on "close" button **/
function closeLeaderboards(){
    if(elements.leaderboards.classList.contains("RevealBoard")){
        toggle2Classes(elements.leaderboards,"HideBoard","RevealBoard");
    }
}
